#!/usr/bin/env bash

#== Bash helpers ==

function info {
  echo " "
  echo "--> $1"
  echo " "
}

#== Provision script ==

info "Provision-script user: `whoami`"

info "Restart web-stack"
service php7.0-fpm restart
service nginx restart
service mysql restart

#info "RabbitMQ plugins enable"
service rabbitmq-server restart
rabbitmq-plugins enable rabbitmq_management
<?php

namespace root\application\models;


use Imagick;
use root\application\logics\ActiveRecord;

class Image extends ActiveRecord
{
    const IMG_SRC_PATH = 'views/img/uploads/src';
    const IMG_THUMB_PATH = 'views/img/uploads/thumb';
    const MAX_FILESIZE = 5000000; // image weight in bytes

    const STATUS_DEF = 0;
    const STATUS_IN_QUEUE = 1;
    const STATUS_DONE = 2;

    private $tableName = 'images';

    public function __construct()
    {
        parent::__construct();
        $this->overrideTableName($this->tableName);
    }

    public static function getClass()
    {
        return get_called_class();
    }

    public function resize()
    {
        if (!is_file($this->getSourcePath(true))) {
            return;
        }

        $pic = $this->getSourcePath(true);
        $image = new Imagick($pic);

        $image->thumbnailImage(100, 0);
        $image->writeImage($this->getThumbPath(true));

        $this->status = self::STATUS_DONE;
        $this->save();
    }

    public function getSourcePath($absolute = false)
    {
        $result = '';
        if ($absolute) {
            $result .= dirname(__DIR__) . '/' ;
        }
        $result .= self::IMG_SRC_PATH . '/' . $this->img_name;
        return $result;
    }

    public function getThumbPath($absolute = false)
    {
        $result = '';
        if ($absolute) {
            $result .= dirname(__DIR__) . '/' ;
        }
        $result .= self::IMG_THUMB_PATH . '/' . $this->img_name;
        return $result;
    }

    public function delete($id = null)
    {
        unlink($this->getSourcePath(true));
        unlink($this->getThumbPath(true));

        return parent::delete();
    }
}

<?php if (!empty($this->imageItem)): ?>
    <form method="POST">
        <input type="hidden" name="id" value="<?=$this->imageItem->id;?>"/>
        <div class="form-group">
            <label>Title:</label>
            <input type="text" name="title" class="form-control" value="<?=$this->imageItem->title;?>"/>
        </div>
        <div class="form-group">
            <label></label>Description:</label>
            <input type="text" name="description" class="form-control" value="<?=$this->imageItem->description;?>"/>
        </div>
        <div class="form-group">
            <input class="btn btn-secondary" type="submit" value="save"/>
        </div>
    </form>
<?php else:?>
    <p>Havn't image like that</p>
<?php endif;?>
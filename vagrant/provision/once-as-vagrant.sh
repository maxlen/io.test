#!/usr/bin/env bash

#== Import script args ==

github_token=$(echo "$1")

#== Bash helpers ==

function info {
  echo " "
  echo "--> $1"
  echo " "
}

#== Provision script ==

info "Provision-script user: `whoami`"

info "Configure composer"
cd /var/www/app
composer config --global github-oauth.github.com ${github_token}
echo "Done!"

info "Install project dependencies (by composer)"
composer --no-progress --prefer-dist install
composer install

echo 'export PATH=/home/vagrant/.config/composer/vendor/bin:$PATH' | tee -a /home/vagrant/.profile

info "Init project"
php init --env=Development --overwrite=y

info "Apply migrations"
#./yii migrate --interactive=0
#./yii_test migrate --interactive=0

info "Create bash-alias '/var/www/app' for vagrant user"
echo 'alias app="cd /var/www/app"' | tee /home/vagrant/.bash_aliases

info "Enabling colorized prompt for guest console"
sed -i "s/#force_color_prompt=yes/force_color_prompt=yes/" /home/vagrant/.bashrc

#info "Install phpMyAdmin"
#cd /var/www/
#sudo composer create-project phpmyadmin/phpmyadmin --repository-url=https://www.phpmyadmin.net/packages.json --no-dev
#sudo chown -R vagrant:vagrant phpmyadmin

<?php

namespace root\application\logics;


use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use root\application\models\Image;

class RabbitManager
{
    private $connection;
    private $channel;

    const RABBIT_QUEUE_NAME = 'images';

    public function __construct()
    {
        $this->connection = new AMQPStreamConnection(
            App::getInstance()->get('rabbitmqHost'),
            App::getInstance()->get('rabbitmqPort'),
            App::getInstance()->get('rabbitmqUser'),
            App::getInstance()->get('rabbitmqPassword')
        );
        $this->channel = $this->connection->channel();
    }

    public function __destruct()
    {
        $this->channel->close();
        $this->connection->close();
    }

    public function putImagesToQueue()
    {
        $images = new Image();
        $imageItems = $images->find(null, ['conditions' => "status = " . Image::STATUS_DEF]);
        foreach ($imageItems as $imageItem) {
            $this->putToQueue($imageItem->id);
            $imageItem->status = Image::STATUS_IN_QUEUE;
            $imageItem->save();
        }
    }

    public function putToQueue($id)
    {
        $this->channel->queue_declare(self::RABBIT_QUEUE_NAME);

        $msg = new AMQPMessage($id, ['delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT]);
        $this->channel->basic_publish($msg, '', self::RABBIT_QUEUE_NAME);
    }

    public function readQueue()
    {
        $callback = function ($msg) {
            $id = $msg->body;
            $images = new Image();
            $image = current($images->find($id));
            if (!empty($image)) {
                $image->resize();
            }
        };

        $this->channel->basic_consume(self::RABBIT_QUEUE_NAME, 'consumer1', false, false, false, false, $callback);
        while (count($this->channel->callbacks)) {
            $this->channel->wait();
        }
    }

    public function stopReadQueue()
    {
        $this->channel->basic_cancel('consumer1');
    }
}
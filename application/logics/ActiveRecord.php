<?php

namespace root\application\logics;

class ActiveRecord
{
    protected $fields = [];
    protected $overrideTableName;

    private $sqlman;

    public function __construct()
    {
        $this->sqlman = new SqlQueryManager();
        $this->sqlman->connect();
    }

    public function __get($field)
    {
        if (array_key_exists($field, $this->fields)) {
            return $this->fields[$field];
        }
    }

    public function __set($field, $value)
    {
        $this->fields[$field] = $value;
    }

    public function __isset($field)
    {
        return isset($this->fields[$field]);
    }

    public function setField($field, $value)
    {
        $this->fields[$field] = $value;
    }

    public function getTableName()
    {
        if (isset($this->overrideTableName)) {
            return $this->overrideTableName;
        } else {
            return get_class($this);
        }
    }

    public function overrideTableName($name)
    {
        $this->overrideTableName = $name;
    }

    public function find($id = null, $params = [])
    {
        $query = "SELECT ";
        if (!empty($params) || !empty($id)) {
            if (!empty($params)) {
                if (isset($params['select'])) {
                    $query .= $params['select'] . " ";
                } else {
                    $query .= "* ";
                }

                if (isset($params['from'])) {
                    $query .= "FROM " . $params['from'] . " ";
                } else {
                    $query .= "FROM " . $this->getTableName() . " ";
                }

                if (!empty($id)) {
                    $query .= "WHERE ";

                    $first = true;

                    if (is_array($id)) {
                        foreach ($id as $ident) {
                            if ($first) {
                                $query .= " id = '" . $ident . "'";
                                $first = false;
                            } else {
                                $query .= " OR id = '" . $ident . "'";
                            }
                        }
                    } else {
                        $query .= " id = {$id}";
                    }

                    if (isset($params['conditions'])) {
                        $first = true;

                        foreach ($id as $ident) {
                            if ($first) {
                                $query .= " id = '" . $ident . "'";
                                $first = false;
                            } else {
                                $query .= " OR id = '" . $ident . "'";
                            }
                        }

                        $query .= " OR " . $params['conditions'];
                    }
                } elseif (isset($params['conditions'])) {
                    $query .= "WHERE {$params['conditions']} ";
                }

                if (isset($params['limit'])) {
                    $query .= " LIMIT " . $params['limit'];
                }

                if (isset($params['order'])) {
                    $query .= " ORDER BY " . $params['order'];
                }
            } else {
                if (is_array($id)) {
                    $query .= " * FROM " . $this->getTableName() . " WHERE";
                    $first = true;

                    foreach ($id as $ident) {
                        if ($first) {
                            $query .= " id = {$ident}";
                            $first = false;
                        } else {
                            $query .= " OR id = {$ident}";
                        }
                    }
                } else {
                    $query .= " * FROM " . $this->getTableName() . " WHERE id = {$id}";
                }
            }
        } else {
            if (isset($params['select'])) {
                $query .= $params['select'] . " ";
            } else {
                $query .= "* ";
            }

            if (isset($params['from'])) {
                $query .= "FROM {$params['from']} ";
            } else {
                $query .= "FROM " . $this->getTableName() . " ";
            }

            if (isset($params['conditions'])) {
                $query .= "WHERE {$params['conditions']}";
            }

            if (isset($params['limit'])) {
                $query .= " LIMIT {$params['limit']}";
            }

            if (isset($params['order'])) {
                $query .= " ORDER BY {$params['order']}";
            }
        }
        
        $recordResult = $this->sqlman->query($query);
        $this->sqlman->close();

        $output = [];

        if (mysqli_num_rows($recordResult) > 0) {
            $output = [];

            while ($varArray = mysqli_fetch_array($recordResult, MYSQLI_ASSOC)) {
                $tempObj = new static;
                foreach ($varArray as $key => $val) {
                    $tempObj->fields[$key] = $val;
                    $tempObj->overrideTableName = $this->overrideTableName;
                }
                $output[] = $tempObj;
            }
        }

        return $output;
    }

    public function delete($id = null)
    {
        if (empty($id) && !empty($this->fields['id'])) {
            $id = $this->fields['id'];
        }

        if (is_array($id)) {
            $table = $this->getTableName();
            $query = "DELETE FROM {$table} WHERE";
            $first = true;
            foreach ($id as $ident) {
                if ($first) {
                    $query .= " id = '" . addslashes($ident) . "'";
                    $first = false;
                } else {
                    $query .= " OR id = '" . addslashes($ident) . "'";
                }
            }
        } else {
            $table = $this->getTableName();
            $query = "DELETE FROM {$table} WHERE id = '" . addslashes($id) . "'";
        }

        $result = $this->sqlman->query($query);
        $this->sqlman->close();
        return $result;
    }

    public function save()
    {
        $table = $this->getTableName();
        if (isset($this->fields['id'])) {
            //update instead
            $query = "UPDATE $table SET";
            foreach ($this->fields as $key => $value) {
                if ($key != 'id') {
                    $query .= " $key = '" . addslashes($value) . "',";
                }
            }
            $query = rtrim($query, ",");
            $query .= " WHERE id = '{$this->fields['id']}'";
            //echo $query;
        } else {
            $newRecord = true;
            $query = "INSERT INTO {$table} (";
            foreach ($this->fields as $key => $value) {
                if ($key == 'id') {
                } else {
                    $query .= "{$key}, ";
                }
            }
            $query = rtrim($query, " ,");
            $query .= ")";
            $query .= " VALUES (";
            foreach ($this->fields as $key => $value) {
                if ($key == 'id') {
                } else {
                    $query .= "'" . addslashes($value) . "',";
                }
            }
            $query = rtrim($query, " ,");
            $query .= ")";
        }

        $result = $this->sqlman->query($query);
        if (!empty($newRecord)) {
            $this->fields['id'] = $this->sqlman->getLastInsertId();
        }
        $this->sqlman->close();

        return $result;
    }

    public function getJson()
    {
        return json_encode($this->fields);
    }
}

?>
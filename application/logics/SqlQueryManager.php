<?php

namespace root\application\logics;

use Exception;
use mysqli;

class SqlQueryManager
{
    private $host;
    private $user;
    private $password;
    private $database;
    private $mysqli;

    public function __construct()
    {
        $this->host = App::getInstance()->get('mysqlHost');
        $this->user = App::getInstance()->get('mysqlUser');
        $this->password = App::getInstance()->get('mysqlPassword');
        $this->database = App::getInstance()->get('mysqlDatabase');
    }

    public function connect()
    {
        $this->mysqli = new mysqli($this->host, $this->user, $this->password, $this->database);
        if ($this->mysqli->connect_error) {
            throw new Exception("Connect failed: " . $this->mysqli->connect_error);
        }
    }

    public function query($query)
    {
        if (isset($this->mysqli)) {
            $result = mysqli_query($this->mysqli, $query);
            return $result;
        } else {
            throw new Exception("ERROR: No Connection to Database.");
        }
    }

    public function getLastInsertId()
    {
        return mysqli_insert_id($this->mysqli);
    }

    public function close()
    {
        if (isset($this->mysqli)) {
            mysqli_close($this->mysqli);
        } else {
            throw new Exception("ERROR: No Connection to Database.");
        }
    }
}

?>
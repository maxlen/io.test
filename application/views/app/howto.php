<ol class="font-weight-bold">
    <li class="mt-5">
        <p>Select and upload images:</p>
        <img src="views/img/doc-step1.png" class="img-responsive" alt="documentation step1">
    </li>
    <li class="mt-5">
        <p>If RabbitMQ doesn't started yet - push on button "turnON Rabbit". You shouldn't do it each time after images loaded.</p>
        <img src="views/img/doc-step2.png" class="img-responsive" alt="documentation step2">
    </li>
    <li class="mt-5">
        <p>all processed images you can see on page List. Now you can update|delete|zoom</p>
        <img src="views/img/doc-step3.png" class="img-responsive" alt="documentation step3">
    </li>
</ol>
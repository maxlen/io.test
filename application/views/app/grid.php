<?php use root\application\models\Image;

if(!empty($this->imageItems)):?>
<table class="table table-hover">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Img</th>
        <th scope="col">Title</th>
        <th scope="col">Description</th>
        <th scope="col">Status</th>
        <th scope="col"></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($this->imageItems as $image):?>
    <tr <?=($image->status < Image::STATUS_DONE) ? 'class="text-muted bg-light"' : '';?>>
        <th scope="row"><?=$image->id;?></th>
        <th>
            <?php if(is_file($image->getThumbPath())): ?>
                <a data-fancybox data-caption="<?=$image->title;?>" href="<?=$image->getSourcePath();?>">
                    <img src="<?=$image->getThumbPath();?>" />
                </a>
            <?php endif;?>
        </th>
        <td><?=$image->title;?></td>
        <td><?=$image->description;?></td>
        <td><?=$image->status;?></td>
        <td>
            <?php if($image->status == Image::STATUS_DONE):?>
                <a class="btn btn-secondary btn-sm" href="/update/<?=$image->id;?>" role="button">update</a>
                <a class="btn btn-outline-secondary btn-sm" href="/delete/<?=$image->id;?>" role="button">delete</a>
            <?php else:?>
                <a class="btn btn-outline-secondary btn-sm" href="/grid" role="button">refresh page</a>
                waiting for Rabbit...
            <?php endif;?>
        </td>
    </tr>
    <?php endforeach;?>
    </tbody>
</table>
<?php else:?>
    <p>There no images</p>
<?php endif;?>
<link  href="/views/css/jquery.fancybox.min.css" rel="stylesheet">
<script src="/views/js/jquery.fancybox.min.js"></script>

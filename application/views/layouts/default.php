<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?= $this->escape($this->pageTitle); ?></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"
          integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
</head>
<body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
        integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
        crossorigin="anonymous"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"
        integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1"
        crossorigin="anonymous"></script>

<header></header>
<nav class="navbar navbar-expand-lg  navbar-dark bg-dark">
    <div class="container">
        <div class="navbar-brand text-secondary">.IO test</div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse ml-5" id="navbarNav">
            <ul class="navbar-nav">
                <?php foreach ($this->actions as $pageName => $innerPage): ?>
                    <li class="nav-item <?= ($innerPage['url'] == $this->action) ? 'active' : ''; ?>">
                        <a class="nav-link" href="<?= $innerPage['url']; ?>"><?= $pageName; ?></a>
                    </li>
                <?php endforeach; ?>
                <li class="nav-item">
                    <a class="btn btn-outline-secondary" href="/startrabbit" role="button">turnON Rabbit</a>
                </li>
                <li class="nav-item ml-2">
                    <a class="nav-link" href="http://<?= $this->domain; ?>:15672" target="_blank">RabbitMQ UI</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<section role="main" class="container mt-3">
    <h1 class""><?= $this->pageTitle; ?></h1>
    <?= $this->yieldView(); ?>
</section>
<footer class="card-footer mt-5">
    <small>&copy; <?= date('Y') ?>. by Maxim Gavrilenko. email: <a
            href="mailto:maxlenash@gmail.com">maxlenash@gmail.com</a></small>
</footer>
</body>
</html>
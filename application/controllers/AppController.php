<?php

namespace root\application\controllers;


use Imagick;
use root\application\logics\App;
use root\application\logics\RabbitManager;
use root\application\logics\UploadImages;
use root\application\models\Image;

class AppController
{
    const DEFAULT_ACTION = 'grid';

    public $data = [];

    public $service;
    public $request;
    public $response;

    public function __construct($service, $request, $response)
    {
        $this->service = $service;

        $this->service->actions = [
            'List' => ['url' => '/grid'],
            'Add image' => ['url' => '/add'],
            'How to' => ['url' => '/howto'],
        ];

        $this->service->action = $request->action;

        $this->service = $service;
        $this->service->domain = App::getInstance()->config->get('domain');
        $this->request = $request;
        $this->response = $response;
    }

    public function actionGrid()
    {
        $this->service->pageTitle = 'List';

        $images = new Image();
        $imageItems = $images->find();
        $this->service->render('views/app/grid.php', ['imageItems' => $imageItems]);
    }

    public function actionAdd()
    {
        $this->service->pageTitle = 'Add Image';

        $data = [];

        if (!empty($_FILES["images"])) {
            $upload = new UploadImages();

            if ($upload->validate()) {
                foreach ($upload->getImages() as $pic) {
                    $i = new Imagick($pic['tmp_name']);
                    $imgName = $upload->genNewImgName() . '.' . $upload->getMime($pic['name']);
                    $i->writeImage(Image::IMG_SRC_PATH . '/' . $imgName);

                    $imgModel = new Image();
                    $imgModel->img_name = $imgName;
                    $imgModel->status = Image::STATUS_DEF;
                    $imgModel->save();
                }

                $worker = new RabbitManager();
                $worker->putImagesToQueue();

                $this->response->redirect('/grid');
            } else {
                $data['error'] = $upload->error['message'];
            }
        }


        $this->service->render('views/app/add.php', $data);

    }

    public function actionUpdate()
    {
        $this->service->pageTitle = 'Update Image Item';

        $imageItem = [];

        if (!empty($this->request->id)) {
            $imageItem = new Image();
            $imageItem = current($imageItem->find($this->request->id));
            if (!empty($imageItem) && $imageItem->status < Image::STATUS_DONE) {
                $this->response->redirect('/grid');
            }
        }

        $shouldSave = false;
        if (!empty($this->request->title) && $this->request->title != $imageItem->title) {
            $shouldSave = true;
            $imageItem->title = trim($this->request->title);
        }

        if (!empty($this->request->description) && $this->request->description != $imageItem->description) {
            $shouldSave = true;
            $imageItem->description = trim($this->request->description);
        }

        if ($shouldSave) {
            $imageItem->save();
            $this->response->redirect('/grid');
        }

        $this->service->render('views/app/update.php', ['imageItem' => $imageItem]);
    }

    public function actionDelete()
    {
        if (!empty($this->request->id)) {
            $image = new Image();
            $imageItem = current($image->find($this->request->id));

            if (!empty($imageItem) && $imageItem->status == Image::STATUS_DONE) {
                $imageItem->delete();
            }
        }

        $this->response->redirect('/grid');
    }

    public function actionHowto()
    {
        $this->service->pageTitle = 'How To';
        $this->service->render('views/app/howto.php');
    }

    public function actionStartrabbit()
    {
        $w = new RabbitManager();
        $w->readQueue();
        $this->response->redirect('/grid');
    }
}
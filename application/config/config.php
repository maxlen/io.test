<?php
return [
    'domain' => 'io.test',
    'mysql' => [
        'host' => 'localhost',
        'database' => 'io_test',
        'user' => 'root',
        'password' => 'vagrant',
    ],
    'rabbitmq' => [
        'host' => 'localhost',
        'port' => 5672,
        'user' => 'root',
        'password' => 'vagrant',
    ],
];
?>
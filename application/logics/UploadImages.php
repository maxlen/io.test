<?php

namespace root\application\logics;

class UploadImages
{
    public $allowImageMimes;
    public $maxSize;
    public $maxFiles;
    public $error;

    function __construct()
    {
        $this->allowImageMimes = ['jpg', 'jpeg', 'png', 'gif'];
        $this->maxSize = (1024 * 1024 * 5);
        $this->maxFiles = 5;
        $this->error = [];
    }

    public function countImages()
    {
        foreach ($_FILES as $file) {
            return count($file["name"]);
        }
    }

    public function getImages()
    {
        $images = [];
        foreach ($_FILES as $file) {
            for ($x = 0; $x < count($file["name"]); $x++) {
                if (empty($file["name"])) {
                    continue;
                }
                $images[] = [
                    "name" => $file["name"][$x],
                    "size" => $file["size"][$x],
                    "tmp_name" => $file["tmp_name"][$x],
                    "type" => $file["type"][$x],
                    "error" => $file["error"][$x]
                ];
            }
        }
        return $images;
    }

    public function validate()
    {
        $images = $this->getImages();

        foreach ($images as $image) {
            $type = $image["type"];
            $typeParts = explode("/", $type);
            $fileType = strtolower(current($typeParts));
            $ext = strtolower($this->getMime($image["name"]));

            $size = $image["size"];

            if (count($images) > $this->maxFiles) {
                $this->error = ["message" => "Too much files! Should be <= {$this->maxFiles}"];
            } elseif ($fileType != 'image' || !in_array($ext, $this->allowImageMimes)) {
                $this->error = ["message" => "Content type is wrong! It should be image. ({$image['name']})"];
            } elseif ($size > $this->maxSize) {
                $this->error = ["message" => "The file is too big! Should be <= {$this->maxSize}. ({$image['name']})"];
            };

            if (!empty($this->error)) {
                break;
            }
        }

        return empty($this->error);
    }

    public function getMime($fileName)
    {
        $partsStr = explode('.', $fileName);
        return end($partsStr);
    }

    public function genNewImgName()
    {
        return uniqid('', true) . '_' . str_shuffle(implode(range('e', 'q')));
    }
}
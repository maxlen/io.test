<?php
require('../vendor/autoload.php');

use root\application\logics\App;

$config = require_once('config/config.php');

$app = App::getInstance($config);
$app->run();

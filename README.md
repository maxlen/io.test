# .io test task

About
------------
Description: very simple pictures storage + Add/Update/Delete/ResizeByQueue:

![Scheme](https://bitbucket.org/maxlen/io.test/raw/674f81d289b7280b0878915f6779b9229dfd0a17/application/views/img/doc-step1.png)

![Scheme](https://bitbucket.org/maxlen/io.test/raw/674f81d289b7280b0878915f6779b9229dfd0a17/application/views/img/doc-step2.png)

![Scheme](https://bitbucket.org/maxlen/io.test/raw/674f81d289b7280b0878915f6779b9229dfd0a17/application/views/img/doc-step3.png)

Install
------------

Recommended using vagrant

#### Manual for Linux/Unix users

1) Install [VirtualBox](https://www.virtualbox.org/wiki/Downloads)

2) Install [Vagrant](https://www.vagrantup.com/downloads.html)

3) Create GitHub [personal API token](https://github.com/blog/1509-personal-api-tokens)

4) Prepare project:
   
```
git clone https://maxlen@bitbucket.org/maxlen/io.test.git
cd io.test/vagrant/config
cp vagrant-local.example.yml vagrant-local.yml
```
   
5) Place your GitHub personal API token to `vagrant-local.yml`

6) Change directory to project root:

```
cd io.test
```

7) Run commands:

```
vagrant plugin install vagrant-hostmanager
vagrant up
```

Thats all. You just need to wait for completion! After that you can access project locally by URLs:

* Site: http://io.test/

   
#### Manual for Windows users

1) Install [VirtualBox](https://www.virtualbox.org/wiki/Downloads)

2) Install [Vagrant](https://www.vagrantup.com/downloads.html)

3) Reboot

4) Create GitHub [personal API token](https://github.com/blog/1509-personal-api-tokens)

5) Prepare project:

   * download [repo](https://bitbucket.org/maxlen/io.test/get/211d7b4f0c3f.zip)
   
   * unzip it
   
   * go into directory `io.test/vagrant/config`
   
   * copy `vagrant-local.example.yml` to `vagrant-local.yml`

6) Place your GitHub personal API token to `vagrant-local.yml`

7) Open terminal (`cmd.exe`), **change directory to project root** and run commands:

```
vagrant plugin install vagrant-hostmanager 
vagrant up
```


(You can read [here](http://www.wikihow.com/Change-Directories-in-Command-Prompt) how to change directories in command prompt) 

Thats all. You just need to wait for completion! After that you can access project locally by URLs:

# Local data

* Main page:  http://io.test/grid
* RabbitMQ:   http://io.test:15672

# HowTo
http://io.test/howto
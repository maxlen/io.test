<?php

namespace root\application\logics;

use Klein\Klein;
use root\application\config\Config;
use root\application\controllers\AppController;

class App
{
    private static $instance;

    public $config;

    protected function __construct($config)
    {
        $this->config = new Config($config);
    }
    
    public static function getInstance($config = [])
    {
        if (static::$instance == null) {
            static::$instance = new self($config);
        }
        return static::$instance;
    }

    public function get($confParam)
    {
        return $this->config->get($confParam);
    }

    public function run()
    {
        $klein = new Klein();

        $klein->respond(function ($request, $response, $service) {
            $service->layout('views/layouts/default.php');
        });

        $klein->respond(
            '[/|/grid|/add|/howto|/update|/delete|/startrabbit:action]?/[i:id]?',
            function ($request, $response, $service) {
                $action = 'action' . ucfirst(str_replace('/', '', $request->action));
                $app = new AppController($service, $request, $response);
                $action = !empty($action) ? $action : $app::DEFAULT_ACTION;
                $app->$action();
            }
        );

        $klein->dispatch();
    }
}
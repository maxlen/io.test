
<?php if (!empty($this->imageResult)): ?>

    <div><?= $this->imageResult;?></div>

<?php endif;?>
<?php if (!empty($this->error)): ?>
    <p class="text-danger mt-3"><?=$this->error;?></p>
<?php endif;?>
<form method="POST" enctype="multipart/form-data">
    <input type="hidden" name="MAX_FILE_SIZE" value="1000000"/>
    <div class="form-group">
        <label for="exampleFormControlFile1">Select files:</label>
        <input type="file" name="images[]" class="form-control-file" accept="image/*" multiple />
    </div>
    <div class="form-group">
        <input class="btn btn-secondary" type="submit" value="upload"/>
    </div>
</form>
<?php

namespace root\application\config;


use stdClass;

class Config
{
    private $data;

    public function __construct($configData)
    {
        $this->data = new stdClass();

        foreach ($configData as $key => $confItem) {
            if (is_string($confItem)) {
                $this->data->$key = $confItem;
                continue;
            }

            foreach ($confItem as $cKey => $cVal) {
                $dataKey = $key . ucfirst($cKey);
                $this->data->$dataKey = $cVal;
            }
        }
    }

    public function get($name)
    {
        if (!isset($this->data->$name)) {
            throw new \Exception("there no config value like '{$name}'");
        }

        return $this->data->$name;
    }
}